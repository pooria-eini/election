from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny

from utils.helpers import ElectionResponse
from .serializers import UserSerializer
from rest_framework import generics


class UserCreate(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny, )

    def create(self, request, *args, **kwargs):
        response = super(UserCreate, self).create(request, *args, **kwargs)
        Token.objects.create(user=response)
        return ElectionResponse(
            data=response.data,
            status=response.status_code
        ).toJsonResponse()
