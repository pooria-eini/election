from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from auth.views import UserCreate

app_name = "auth"

urlpatterns = [
    path('register/', UserCreate.as_view(), name="register"),
    path('authenticate/', obtain_auth_token)
]
