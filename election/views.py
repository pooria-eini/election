from django.utils import timezone
from rest_framework.exceptions import ValidationError
from rest_framework.generics import UpdateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from election.models import Election, Candidate
from election.serializers import ElectionSerializer, PublicElectionSerializer, CandidateSerializer
from utils.helpers import ElectionResponse


class ElectionsViewSet(ModelViewSet):
    queryset = Election.objects.all()
    serializer_class = PublicElectionSerializer

    def retrieve(self, request, *args, **kwargs):
        response = super(ElectionsViewSet, self).retrieve(request, *args, **kwargs)
        return ElectionResponse(
            data=response.data,
            status=response.status_code,
        ).toJsonResponse()

    def list(self, request, *args, **kwargs):
        response = super(ElectionsViewSet, self).list(request, *args, **kwargs)
        return ElectionResponse(
            data=response.data,
            status=response.status_code
        ).toJsonResponse()


class VoteAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, *args, **kwargs):
        election = Election.objects.filter(id=self.kwargs.get("election_id")).last()
        if self.request.user.is_staff or self.request.user.is_superuser:
            raise ValidationError("Superusers and staffs can't vote")
        if timezone.now() < election.start_time:
            raise ValidationError("Election is not started yet")
        if timezone.now() > election.end_time:
            raise ValidationError("Election is finished")

        if self.request.user.id in election.voters.values_list("id", flat=True):
            raise ValidationError("You have already voted")

        election.voters.add(self.request.user)
        candidate = Candidate.objects.filter(
            id=self.kwargs.get("candidate_id"),
            election=election
        ).last()
        if not candidate:
            raise ValidationError("No such candidate for this election")
        candidate.votes_count += 1
        candidate.save()
        response = {
            "message": "رای شما با موفقیت ثبت شد",
            "votes_count": candidate.votes_count
        }
        return ElectionResponse(data=response, status=201).toJsonResponse()


class ElectionManagerAPIView(ElectionsViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = ElectionSerializer


class CandidateUpdateAPIView(RetrieveAPIView, UpdateAPIView):
    serializer_class = CandidateSerializer
    queryset = Candidate.objects.all()

    def get_object(self):
        candidate = super(CandidateUpdateAPIView, self).get_object()
        if self.request.user.id != candidate.user_id:
            raise ValidationError(
                "You can't update another candidate's data"
            )
        return candidate

    def update(self, request, *args, **kwargs):
        response = super(
            CandidateUpdateAPIView, self
        ).update(request, *args, **kwargs)
        return ElectionResponse(
            data=response.data,
            status=response.status_code
        ).toJsonResponse()

    def retrieve(self, request, *args, **kwargs):
        response = super(
            CandidateUpdateAPIView, self
        ).retrieve(request, *args, **kwargs)
        return ElectionResponse(
            data=response.data,
            status=response.status_code
        ).toJsonResponse()



