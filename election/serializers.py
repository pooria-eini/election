from django.contrib.auth.models import User
from rest_framework import serializers

from election.models import Election, Candidate


class PublicElectionSerializer(serializers.ModelSerializer):
    candidates = serializers.SerializerMethodField()

    def get_candidates(self, instance):
        return instance.candidates.values_list("user__username", "id")

    class Meta:
        model = Election
        fields = (
            "start_time",
            "end_time",
            "name",
            "id",
            "candidates"
        )


class ElectionSerializer(serializers.ModelSerializer):
    voters = serializers.SerializerMethodField()
    candidates = serializers.SerializerMethodField()

    def get_voters(self, instace):
        return instace.voters.values_list("username", flat=True)

    def get_candidates(self, instance):
        return instance.candidates.values_list("user__username", "id")

    class Meta:
        model = Election
        fields = (
            "voters",
            "start_time",
            "end_time",
            "name",
            "id",
            "candidates"
        )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("first_name", "last_name")


class CandidateSerializer(serializers.Serializer):
    user = UserSerializer()
    username = serializers.SerializerMethodField(read_only=True)
    degree = serializers.CharField(max_length=1)
    age = serializers.IntegerField()

    def get_username(self, instance):
        return instance.user.username

    def update(self, instance, validated_data):
        user_data = validated_data.pop("user", {})
        degree = validated_data.get("degree")
        age = validated_data.get("age")
        UserSerializer().update(
            instance=instance.user,
            validated_data=user_data
        )
        if degree:
            instance.degree = validated_data.pop("degree")
        if age:
            instance.age = validated_data.pop("age")
        instance.save()
        return instance
