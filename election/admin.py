from django.contrib import admin

# Register your models here.
from election.models import Election, Candidate

admin.site.register(Election)
admin.site.register(Candidate)
