from django.urls import path

from election.views import ElectionsViewSet, VoteAPIView, ElectionManagerAPIView, CandidateUpdateAPIView

app_name = "elections"

urlpatterns = [
    path("elections/", ElectionsViewSet.as_view({"get": "list", "post": "create"}), name="list-create"),
    path("elections/<int:pk>/", ElectionsViewSet.as_view({"get": "retrieve"}), name="retrieve-update"),

    path("manager-elections/", ElectionManagerAPIView.as_view({"get": "list"}), name="list"),
    path("manager-elections/<int:pk>/", ElectionManagerAPIView.as_view({"get": "retrieve"}), name="retrieve"),
    path("candidate/<int:pk>/", CandidateUpdateAPIView.as_view(), name="candidate_update"),

    path("vote/<int:election_id>/<int:candidate_id>/", VoteAPIView.as_view(), name="vote"),
]
