# Generated by Django 3.1.6 on 2021-02-10 14:03

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('election', '0002_auto_20210210_1402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='election',
            name='voters',
            field=models.ManyToManyField(blank=True, related_name='elections', to=settings.AUTH_USER_MODEL),
        ),
    ]
