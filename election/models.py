from uuid import uuid4

from django.contrib.auth.models import User
from django.db import models


class Election(models.Model):
    voters = models.ManyToManyField(User, related_name="elections", blank=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    name = models.CharField(max_length=64)

    class Meta:
       verbose_name = "انتخابات"
       verbose_name_plural = "انتخابات"

    def __str__(self):
       return self.name


class Candidate(models.Model):
   DEGREE_DIPLOMA = "1"
   DEGREE_ASSOCIATE = "2"
   DEGREE_BACHELOR = "3"
   DEGREE_MASTER = "4"
   DEGREE_DOCTORAL = "5"
   DEGREE_POST_DOCTORAL = "6"

   DEGREE_CHOICES = [
       (DEGREE_DIPLOMA, "دیپلم"),
       (DEGREE_ASSOCIATE, "کاردانی"),
       (DEGREE_BACHELOR, "کارشناسی"),
       (DEGREE_MASTER, "ارشد"),
       (DEGREE_DOCTORAL, "دکتری"),
       (DEGREE_POST_DOCTORAL, "فوق دکتری"),
   ]
   user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="candidates")
   code = models.UUIDField(default=uuid4)
   degree = models.CharField(max_length=1, choices=DEGREE_CHOICES)
   age = models.IntegerField()
   votes_count = models.IntegerField(default=0)
   election = models.ForeignKey(Election, related_name="candidates", on_delete=models.CASCADE)

   class Meta:
       verbose_name = "کاندید"
       verbose_name_plural = "کاندیدها"

   def __str__(self):
       return str(self.user) + " " + str(self.code)

   def get_first_name(self):
       return self.user.first_name

   def get_last_name(self):
       return self.user.last_name