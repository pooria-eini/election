import json

from rest_framework import status as rest_status
from rest_framework.response import Response


class ElectionResponse:
    def __init__(
        self,
        data,
        status="ok",
        status_header=rest_status.HTTP_200_OK,
        message="",
        message_code="",
        headers=None,
    ):
        self.status = status
        self.headers = headers
        self.status_header = status_header
        self.message = message
        self.message_code = message_code
        self.data = data

    def toJson(self):
        result = self.__dict__
        result.pop("status_header", None)
        return json.dumps(self.__dict__)

    def toJsonResponse(self):
        result = self.__dict__
        status_header = result.pop("status_header", None)
        headers = result.pop("headers", None)
        if not bool(headers):
            headers = {}
        return Response(status=status_header, data=result, headers=headers)
